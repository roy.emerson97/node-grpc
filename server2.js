const grpc = require('grpc');
const protoNotes = grpc.load('notes.proto');
const uuid = require('uuid/v1');

const notes = [
  { id: '1', title: 'Note 1', content: 'Content 1'},
  { id: '2', title: 'Note 2', content: 'Content 2'}
]

const server = new grpc.Server();

server.addService(protoNotes.NoteService.service, {
  getNotesList: (_, callback) => {
    console.log('Return list');
    callback(null, notes)
  },
  insertNote: (call, callback) => {
    console.log('Inserting note', call.request);
    let note = call.request;
    note.id = uuid();
    notes.push(note);
    callback(null, note);
  },
  deleteNote: (call, callback) => {
    const noteId = call.request.id;
    console.log('Deleting note with id', noteId);
    const noteExists = notes.some(note => note.id === noteId);
    if (noteExists) {
      notes.filter(note => note.id !== noteId)
      callback(null, {});
    } else {
      callback({
        code: grpc.status.NOT_FOUND,
        details: "Not Found"
      });
    }
  }
});

server.bind('127.0.0.1:50052',
  grpc.ServerCredentials.createInsecure()
)

console.log('Server is running at http://127.0.0.1:50052');
server.start();