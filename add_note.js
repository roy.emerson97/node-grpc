const client = require('./client');

const newNote = {
  title: "New Note",
  content: "New Note content"
};

client.insertNote(newNote, (error, note) => {
  if (!error) {
    console.log('New Note created successfully', note)
  } else {
    console.error(error)
  }
});